const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: '.'
    },
    notify: false
  });
});

gulp.task('sass', function() {
  return gulp
    .src('./scss/**/*.scss')
    .pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
    .pipe(postcss())
    .pipe(gulp.dest('./css'));
});

gulp.task('watch', ['browser-sync'], function() {
  gulp.watch('scss/**/*.scss', ['sass']);
  gulp.watch('css/**/*.css', browserSync.reload);
  gulp.watch('*.html', browserSync.reload);
});

gulp.task('default', ['watch']);
